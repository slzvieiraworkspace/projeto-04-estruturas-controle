
public class ExemploWhile {

    public static void main(String[] args) {
        
        int contador = 1;
        
        while (contador < 100) {
            System.out.println("Valor da vez: " + contador);
            contador += 5;
        }
    }
}
