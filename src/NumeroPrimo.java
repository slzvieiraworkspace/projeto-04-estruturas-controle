public class NumeroPrimo {
 
	public static void main(String[] args) {
	
		int numero = 5;
		int qtdDivisor = 0;
		
		
		for (int i = 1;  i <= numero; i++) {
		
			int divisor = numero % i;
					
			if (divisor == 0 ){
				
					qtdDivisor++;
				}
			
			}
		
		if(qtdDivisor > 2 ) {
			
			System.out.println("este numero não é primo." + numero);
			
		} else {
			System.out.println("este numero é primo : " + numero);
		}
	
	}
}