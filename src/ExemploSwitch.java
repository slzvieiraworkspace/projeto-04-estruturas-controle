
public class ExemploSwitch {

    public static void main(String[] args) {
        
        String pais = "Brasil";
        
        switch (pais) {
            case "México":
            case "Argentina":
            case "Espanha":
                System.out.println("Buenos días");
                break;
            case "França":
                System.out.println("Bon jour");
                break;
            case "Itália":
                System.out.println("Bon jorno");
                break;
            case "Brasil":
            case "Portugal":
                System.out.println("Bom dia");
                break;
            default:
                System.out.println("Good morning");
        }
    }
}
