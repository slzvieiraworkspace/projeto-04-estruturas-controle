import java.util.Random;

public class ExemploContinue {

    public static void main(String[] args) {
        
        for (int i = 0; i < 10; i++) {

            int numeroDaSorte = sorteiaNumero();
            
            if (numeroDaSorte < 100) {
                System.out.println("Ignorando sorteio: " + numeroDaSorte);
                continue;
            }
            
            System.out.println(numeroDaSorte);
        }
    }
    
    static int sorteiaNumero() {
        return new Random().nextInt(1000) + 1;
    }
    
    static void exemploTosco() {
        
        for (int i = 0; i < 10; i++) {
            
            int val1 = 0, val2 = 0, val3 = 0, val4 = 0;
            
            if (val1 <= 10) {
                continue;
            }

            if (val2 >= 15) {
                continue;
            }

            if (val3 <= 10) {
                continue;
            }

            if (val4 >= 15) {
                continue;
            }
            
            System.out.println("Faça algo!!");
        }
    }
}
