import java.util.Random;

public class ExemploBreak {

    public static void main(String[] args) {
        
        for (int i = 0; i < 10; i++) {

            int numeroDaSorte = sorteiaNumero();
            
            if (numeroDaSorte < 100) {
                System.out.println("Abandonando sorteio: " + numeroDaSorte);
                break;
            }
            
            System.out.println(numeroDaSorte);
        }
    }
    
    static int sorteiaNumero() {
        return new Random().nextInt(1000) + 1;
    }
}
